# Databricks notebook source
##be sure to run your Secrets file here if using databricks! Otherwise this is all python, and can be run as a python script.

# COMMAND ----------

##General utilities
import datetime as dt
import os
import sys
import random
import time
from itertools import cycle
from uuid import uuid4
import requests
from pprint import pprint
from multiprocessing.pool import ThreadPool
import os, os.path
import numpy as np
import requests, json
import re
import json
from pyspark.sql import Row
from pyspark.sql.window import Window
import random
from datetime import datetime
import time
#from matplotlib import pyplot as plt
import matplotlib.pyplot as plt
import pandas as pd



from sklearn.metrics import confusion_matrix
from sklearn.metrics import multilabel_confusion_matrix
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score
from sklearn.metrics import precision_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_classification
from sklearn.datasets import make_blobs
from sklearn.metrics import auc

# COMMAND ----------

##this function is so we can plot the active learning method vs the base method. base uses (in our default) 10 batches of size 1000 data points
##meanwhile the active learning method does 100 datapoints each time for each single set of 1000, so we need to remap some indexes so we can plot them
##for fair comparision
def fill_list(basic_method_list_to_expand,active_learning_indexes):
  new_list=[]
  for i in active_learning_indexes:
    print(i)
    #if i is 0:
      #new_list.append(basic_method_list_to_expand[0])
    if i is 1:
      new_list.append(basic_method_list_to_expand[0])
    elif 2 <= i <= 2.9:
      new_list.append(basic_method_list_to_expand[1])
    elif 3 <= i <= 3.9:
      new_list.append(basic_method_list_to_expand[2])
    elif 4 <= i <= 4.9:
      new_list.append(basic_method_list_to_expand[3])
    elif 5 <= i <= 5.9:
      new_list.append(basic_method_list_to_expand[4])
    elif 6 <= i <= 6.9:
      new_list.append(basic_method_list_to_expand[5])
    elif 7 <= i <= 7.9:
      new_list.append(basic_method_list_to_expand[6])
    elif 8 <= i <= 8.9:
      new_list.append(basic_method_list_to_expand[7])
    else:
      new_list.append(basic_method_list_to_expand[8])
      
  return new_list

# COMMAND ----------

#### other basic parameters ####
#### you need at least 300-3000 total_runs according to:https://modelassist.epixanalytics.com/display/EA/The+minimum+number+of+iterations+one+can+run+in+a+Monte+Carlo+simulation#:~:text=The%20minimum%20number%20of%20iterations%20one%20can%20run%20in%20a%20Monte%20Carlo%20simulation,-Skip%20to%20end&text=A%20short%20answer%2C%20burdened%20with,%22no%20less%20than%20300'.

####set here your parameters for your monte carlo sim.
active_wins=0
total_runs=3000  
use_toy_data=0
use_synth_data=1
use_blob_data=0
k=100
remove_n = 1
get_even=0
run_count=1
splits=10
n_cv=2
col_use=1
n_size_dif=10
a_win=[]
n_samp=10000
round_improved=[]

# COMMAND ----------

for run in range(total_runs):
  
  ################################ Selecting what dataset to use, Option 1: Toy Datasets from scikit-learn ################################
  if use_toy_data==1:
    from sklearn import datasets

    def sklearn_to_df(sklearn_dataset):
        df = pd.DataFrame(sklearn_dataset.data, columns=sklearn_dataset.feature_names)
        df['target'] = pd.Series(sklearn_dataset.target)
        return df

    df = sklearn_to_df(datasets.load_iris())
    df.head()
  
  ################################ Selecting what dataset to use, Option 2: Synth Datasets from scikit-learn make_classification ################################
  if use_synth_data == 1:
    #plt.figure(0)
    #plt.title("Multi-class, two informative features, one cluster, run #: "+str(run_count),
    #          fontsize='small')
    X1, Y1 = make_classification(n_samples=n_samp,n_features=2, n_redundant=0, n_informative=2,
                                 n_clusters_per_class=1, n_classes=3)
    #plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1,
    #            s=25, edgecolor='k')

    df=pd.DataFrame(X1,columns =['Feature1', 'Feature2'])
    df['target']=Y1
    df.head()
  
  ################################ Selecting what dataset to use, Option 3: Synth Datasets from scikit-learn make_blobs ################################
  if use_blob_data==1:
    #plt.figure(0)
    #plt.title("Three blobs, run # " +str(run_count), fontsize='small')
    X1, Y1 = make_blobs(n_samples=n_samp,n_features=2, centers=3)
   # plt.scatter(X1[:, 0], X1[:, 1], marker='o', c=Y1,
    #            s=25, edgecolor='k')
    print(X1)
    print(Y1)
    df=pd.DataFrame(X1,columns =['Feature1', 'Feature2'])
    df['target']=Y1
    df.head()  
   
  
  ################################ get random samples for pipeline 1, the basic batch way ################################
  
  if get_even==1:
    n_splits = len(df)/k
    shuffled = df.sample(frac=1)
    result = np.array_split(shuffled, n_splits)  
    print('amount of data per split is:', len(result[0]))
  else:
    shuffled = df.sample(frac=1)
    result = np.array_split(shuffled, splits) 
    print('amount of data per split is:', len(result[0]))
  for part in result:
     print(part,'\n')
  
  ################################ Basic method ################################
  
  df_1=pd.DataFrame()

  count=1
  scores_basic_way = {}#[]
  models_basic_way=[]

  for item in result:
    #### append data to our set ####
    df_1=df_1.append(item,ignore_index=True)
    #print('this round of data is using: ', len(df_1), ' datapoints')

    #### select our data for training ####
    X1 = df_1.iloc[:,col_use]#df_1.drop(axis=0, columns=['target'])
    Y1 = df_1.target
    X_train1, X_test1, y_train1, y_test1 = train_test_split(X1, Y1, test_size=0.33, random_state=42)
    model1 = SVC( random_state=np.random.RandomState(123), probability=True)
    models_basic_way.append(model1)
    data1=np.array(X_train1).reshape((len(X_train1), 1))
    model1.fit(data1, y_train1)

    #### get scores from this model and save out ####
    scores1 = cross_val_score(model1, data1, y_train1, cv=n_cv)

    #print((scores1))

    print('The round is ', count, '. Average of ', str(n_cv), ' fold cv is:' , sum(scores1)/len(scores1), 'for a model built with ', len(df_1), '# of datapoints' )
    #scores_basic_way.append({count: sum(scores1)/len(scores1)})
    scores_basic_way[count]= sum(scores1)/len(scores1)
    count=count+1

  print(scores_basic_way)
  
  ################################ Active Learning Method ################################

  #### we're using batches that are 10 times smaller than what the basic way does. ####
  #### we need then to do many more rounds of training to add up to the same amount of data, so this will keep things aligned ####

  n_small_batch = int(len(result[0])/n_size_dif)
  count_ratio = (1/n_size_dif) 

  #### build the df to predict on ####
  to_infer_pdf= pd.DataFrame()
  for item in result[1:-1]:
    to_infer_pdf=to_infer_pdf.append(item,ignore_index=True)
    #print(len(to_infer_pdf))

  print('infer pdf built')

  #### we use the same baseline so count starts at 2, and the first score is the same ####
  models_active_way=[] 
  count=2 
  scores_active_way={1:scores_basic_way[1]} 
  df_2I=result[0]

  try:
    #### for loop starts here ####
    for i in range(len(result[1:]*n_size_dif)):
     #### here we get the probabilities using the base model ####

      X_test2I=to_infer_pdf.iloc[:,col_use]
      dataI2=np.array(X_test2I).reshape((len(X_test2I), 1))
      if count is 2:
        prediction_of_probability = models_basic_way[0].predict_proba(dataI2)
      else:
        prediction_of_probability = model2I.predict_proba(dataI2)
      col_list=[]
      for i in range(prediction_of_probability.shape[1]):
        to_infer_pdf['prob_class'+ str(i)+'_r'+str(count)]=prediction_of_probability[:,i]
        col_list.append('prob_class'+ str(i)+'_r'+str(count))




      #to_infer_pdf['prob_class_1_r'+str(count)]=prediction_of_probability[:,1]
      #to_infer_pdf['prob_class_2_r'+str(count)]=prediction_of_probability[:,2]


     # to_infer_pdf['Most_Likely_Class_r'+str(count)]=to_infer_pdf.apply(lambda x: get_max_class(x['prob_class_0_r'+str(count)], x['prob_class_1_r'+str(count)], x['prob_class_2_r'+str(count)]), axis=1)
     # to_infer_pdf['Most_Likely_Class_Prob_r'+str(count)]=to_infer_pdf.apply(lambda x: get_max_class_prob(x['prob_class_0_r'+str(count)], x['prob_class_1_r'+str(count)], x['prob_class_2_r'+str(count)]), axis=1)
      to_infer_pdf['Most_Likely_Class_Prob_r'+str(count)] =to_infer_pdf[col_list].max(axis=1)
      # to_infer_pdf[col_list].max(axis=1)


      #then we get the smallest confidence, to review
      to_review2=to_infer_pdf.nsmallest(n_small_batch, 'Most_Likely_Class_Prob_r'+str(count))

      #appebd to base data
      df_2I=df_2I.append(to_review2)

      # split train input and output data
      X2I = df_2I.iloc[:,0]#
      Y2I = df_2I.target
      print('amount of training data', len(X2I))
      X_train2I, X_test2I, y_train2I, y_test2I = train_test_split(X2I, Y2I, test_size=0.33, random_state=42)


      model2I = SVC( random_state=np.random.RandomState(123), probability=True)
      models_active_way.append(model2I)
      data2I=np.array(X_train2I).reshape((len(X_train2I), 1))
      model2I.fit(data2I, y_train2I)

      scores2I = cross_val_score(model2I, data2I, y_train2I, cv=n_cv)#,
      #scores_active_way.append({count: sum(scores2I)/len(scores2I)})
      scores_active_way[count]=sum(scores2I)/len(scores2I)


      to_infer_pdf=to_infer_pdf.drop(to_review2.index)
      #print(len(to_infer_pdf))
      print('The round is ', count, '. Average of ', str(n_cv), ' fold cv is:' , sum(scores2I)/len(scores2I), 'for a model built with ', len(df_2I), '# of datapoints' )
      count=round(count+count_ratio, 1)
  except:
    pass
  print('done with round', run_count)
  ################################ Plotting our data ################################ 
  
  ##right now this plotting is commented out, for large sims it is too much to handle, but you can see it for a smaller sim if you reduce total_runs
  # line 1 points
  x1 = list(scores_basic_way.keys())
  y1 = list(scores_basic_way.values())
  # plotting the line 1 points 
  #plt.figure(1)
  #plt.plot(x1, y1, label = "Basic Approach")
  # line 2 points
  x2 = list(scores_active_way.keys())
  y2 = list(scores_active_way.values())
  # plotting the line 2 points 
  #plt.plot(x2, y2, label = "Active Learning Approach")
  
  #plt.xlabel('Batches of Data')
  # Set the y axis label of the current axis.
  #plt.ylabel('Cross Validation Scores')
  # Set a title of the current axes.
  #plt.title('Traditional vs Active Learning Approach to Model Cross Validation Score, Run #'+ str(run_count))
  # show a legend on the plot
  #plt.legend()
  # Display a figure.
  #plt.show()
  
  
  ################################ Calculate the area under curve, to see if we suceeded in doing better w/active learning ################################


  basic_auc=auc(x1,y1)
  active_auc= auc(x2,y2)
  print('computed AUC for basic method using sklearn.metrics.auc: {}'.format(basic_auc))
  print('computed AUC for active learning method using sklearn.metrics.auc: {}'.format(active_auc))

  if active_auc>basic_auc:
        print('active learning did better')
        active_wins=active_wins+1
        a_win.append(1)
  else:
    a_win.append(0)
  
  
  
  ################################ Plotting with extrapolated indexes, this is just a check ################################
  test=fill_list(y1,x2)
  #plt.figure(2)
  #plt.plot(x2, test, label = "Basic Approach")
  # line 2 points
  #x2 = list(scores_active_way.keys())
  #y2 = list(scores_active_way.values())
  # plotting the line 2 points 
  #plt.plot(x2, y2, label = "Active Learning Approach")
  
  #plt.xlabel('Batches of Data')
  # Set the y axis label of the current axis.
  #plt.ylabel('Cross Validation Scores')
  # Set a title of the current axes.
  #plt.title('Traditional vs Active Learning Approach to Model Cross Validation Score, Extrapolated Indexes, Run #'+ str(run_count))
  # show a legend on the plot
  plt.legend()
  # Display a figure.
  plt.show()
  
  
  ################################ Get the round it improved so we can do analysis ################################
  if active_auc>basic_auc:
    check_df=pd.DataFrame()
    check_df['Basic_Method']=test
    check_df['Active_Method']=y2
    check_df['Batch_Round']=x2
    check_df=check_df[check_df.Active_Method.gt(check_df.Basic_Method)]
    check_df['diffs']=check_df.Batch_Round.diff()
    check_df=check_df.sort_values(by=['diffs', 'Batch_Round'],ascending=True).reset_index(drop=True) #this method may not work if a
    #mask=check_df.diffs.where(check_df.diffs == float(0.1))

    #check_df.head()

    round_improved.append(check_df.Batch_Round[0])
  else:
     round_improved.append(0)
  run_count=run_count+1
  print('on run #:', run_count)
    

# COMMAND ----------

print('% of active wins:', active_wins/total_runs)
print('average round where we saw sustained improvement', round(sum(round_improved)/len(round_improved),1))

# COMMAND ----------


