# Databricks notebook source
## run your Secrets file here if you're using databricks! This is all python so you can run w/o databricks as well

# COMMAND ----------

#### General utilities ####
import datetime as dt
import os
import sys
import random
import time
from itertools import cycle
from uuid import uuid4
import requests
from pprint import pprint
from multiprocessing.pool import ThreadPool
import os, os.path
import numpy as np
import requests, json
import re
import json
from pyspark.sql import Row
from pyspark.sql.window import Window
import random
from datetime import datetime
import time
from matplotlib import pyplot as plt
import pandas as pd
##Labelbox utilities
import labelbox #as lb
from labelbox import Project, Dataset
from labelbox.schema.bulk_import_request import BulkImportRequest
from labelbox.schema.enums import BulkImportRequestState
from labelbox import Client, Project, schema

# COMMAND ----------

#### Loading training data #####

#### this particular use-case uses data from a coworker's project. The goal of the project was to find out which items needed review, using the model
#### and setting some custom logic based on the project needs to produce a list at the end for review
#### Provide your own data here to run the pipeline
import pandas as pd
import s3fs
train_path='s3://chegg-ds-data/sbhargava/deck_to_topics_annotations_Labelbox/input_data_forreview_forJasmine/train_data_wembeddings.parquet'
df_train= pd.read_parquet(train_path)

display(df_train)

# COMMAND ----------

#### Loading testing data #####

test_path='s3:///chegg-ds-data/sbhargava/deck_to_topics_annotations_Labelbox/input_data_forreview_forJasmine/test_data_wembeddings.parquet'
df_test= pd.read_parquet(test_path)


display(df_test)

# COMMAND ----------

#df_train[['top_level_topic1','top_level_topic2', 'top_level_topic3']] = pd.DataFrame(df_train.top_level_topic.tolist(), index= df_train.index)
#df_train.head()

# COMMAND ----------

#### Getting the training label space encoded for small model training ####

courses= []
courses.extend(df_train.majority_toplvl_topic.to_list())


#### Filter out to only unique values ####
courses=list(set(courses))


#### Now make a mapping so we can train the model ####
courses_dict = { courses[i]:i  for i in range(0, len(courses) ) }
print(courses_dict)

# COMMAND ----------

#### Apply the mapping to our training data ####
df_train['API_Labeled_Top_Topic1_Encoding']=df_train.majority_toplvl_topic.map(courses_dict)

df_train.head()

# COMMAND ----------

#### Prepping the training data for the model ####

from sklearn.model_selection import train_test_split
 
train, test = train_test_split(df_train, test_size=0.05, random_state=123)
X_train = train.deck_vector
X_test = test.deck_vector
y_train = train.API_Labeled_Top_Topic1_Encoding
y_test = test.API_Labeled_Top_Topic1_Encoding

# COMMAND ----------

from sklearn.metrics import confusion_matrix
from sklearn.metrics import multilabel_confusion_matrix
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score
from sklearn.metrics import precision_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV


#### Optimize the model ####

n_estimators = [100, 300, 500, 800, 1200]
max_depth = [5, 8, 15, 25, 30]
min_samples_split = [2, 5, 10, 15, 100]
min_samples_leaf = [1, 2, 5, 10] 
forest = RandomForestClassifier(random_state = 1)
hyperF = dict(n_estimators = n_estimators, max_depth = max_depth,  
              min_samples_split = min_samples_split, 
             min_samples_leaf = min_samples_leaf)

gridF = GridSearchCV(forest, hyperF, cv = 2, verbose = 1, 
                      n_jobs = -1)
bestF = gridF.fit(list(np.array(X_train).reshape(X_train.shape[0],)), y_train)

# COMMAND ----------

#### Get best params ####

print(bestF.best_params_)
max_depth_val= bestF.best_params_['max_depth']
min_samples_leaf_val= bestF.best_params_['min_samples_leaf']
min_samples_split_val= bestF.best_params_['min_samples_split']
n_estimators_val= bestF.best_params_['n_estimators']


# COMMAND ----------

#### Build the classifer, and use cross val to assess.  We determined for this dataset this type of classifer works best ####

model1=RandomForestClassifier(max_depth= max_depth_val, min_samples_leaf= min_samples_leaf_val, min_samples_split= min_samples_split_val, n_estimators= n_estimators_val)
model1.fit(list(np.array(X_train).reshape(X_train.shape[0],)), y_train)
scores = cross_val_score(model1, list(np.array(X_train).reshape(X_train.shape[0],)), y_train, cv=2)#, cv=n_cv)


print((scores))
print('average of 2 fold cv is:', sum(scores)/len(scores) )


# COMMAND ----------

#### Functions to use the model ####

from itertools import chain

def get_probs(embeds):

  test_keys=model1.classes_
  test_values=model1.predict_proba(embeds.reshape(1, -1))
  test_values=list(chain(*test_values))
  test_keys_decrypt=[]
  for name, clasz in courses_dict.items():  
    for key in test_keys:
      if clasz == key:
        test_keys_decrypt.append(name)
        #print(key)
  
  return(dict(zip(test_keys_decrypt, test_values)) )


def get_class(embeds):
  class_enc= model1.predict(embeds.reshape(1,-1))[0]
  
  for name, clasz in courses_dict.items():  
    if clasz == class_enc:
      return name


def get_class_prob(small_class, prob_dict):
  return {small_class:prob_dict[small_class]}

def get_class_proba(small_class, prob_dict):
  return prob_dict[small_class]


#### Get the various outputs we need using the model ####

df_test['Small_Model_Class']=df_test.deck_vector.apply(get_class)
df_test['Small_Model_Probs']=df_test.deck_vector.apply(get_probs)
df_test['Small_Model_Class_Prob_Dict']=df_test.apply(lambda x: get_class_prob(x.Small_Model_Class, x.Small_Model_Probs), axis=1)
df_test['Small_Model_Class_Probability']=df_test.apply(lambda x: get_class_proba(x.Small_Model_Class, x.Small_Model_Probs), axis=1)
df_test.head()

# COMMAND ----------

#### Get mode probability + class ####

df_roll=df_test.groupby('External ID').agg({ 'Small_Model_Class':lambda x: x.value_counts().index[0],  'Small_Model_Class_Probability': lambda x: x.value_counts().index[0]})

#df_roll=df_test.groupby('External ID').agg({ 'Small_Model_Class':np.median,  'Small_Model_Class_Probability': np.median})

df_roll=df_roll.rename(columns={"Small_Model_Class_Probability": "Mode_Small_Model_Class_Probability","Small_Model_Class": "Mode_Small_Model_Class"})

df_roll

# COMMAND ----------

#### Merge with our other dataset so we have everything we need ####

result=pd.merge(df_roll, df_test, on='External ID', how='inner') 
result.head()

# COMMAND ----------

#### Function to get review scores ####

### this is custom logic for this particular use case. Implement your own for your particular project if needed ####

def check_review(mode_small_model_class, mode_small_model_prob,sme_top_level_topic_list, thresh):
  threshold=thresh
  #Is it high or low confidence?
  if mode_small_model_prob> threshold:
  
    #print('high confidence prediction')
    
    #is the prediction in any of the SME labels?
    if any(x == mode_small_model_class for x in sme_top_level_topic_list):
     # print('high confidence prediction and model is in one of the sme labels')
     #are the sme labels in the training data?
      if len(set(sme_top_level_topic_list).intersection(set(courses))) > 0: 
        print('high confid, model in sme labels, and in training space, LOW Priority')
        return 'LOW'
      #else:
      #  return
     #the prediction is not in any of the SME labels 
    else:
      #print('high confidence prediction and mode is NOT in SME labels')
      if len(set(sme_top_level_topic_list).intersection(set(courses))) > 0: 
        print('high confid, model  in sme labels, and  in training space, HIGH Priority')
        return 'HIGH'
      else:
        return 'HEURISTICS NEEDED'
       #if so, is the sme labels in the training data
      
    
  elif mode_small_model_prob< threshold:  
   # print('low confidence prediction')
    if any(x == mode_small_model_class for x in sme_top_level_topic_list):
     # print('low confidence prediction and model is in one of the sme labels')
      if len(set(sme_top_level_topic_list).intersection(set(courses))) > 0: 
        print('low confid, model in sme labels, and in training space, LOW Priority')
        return 'LOW'
      else:
        return 'HEURISTICS NEEDED'
    else:
      return 'HEURISTICS NEEDED'
   


# COMMAND ----------

#### Now make sure only 20% of items go for review ####

#### Calculate what is 20% of test data ####
Decks_Needed = round(len(df_roll.index)*.20,0)
print('Max amount to go for review: ', Decks_Needed)

#### Initial thresholds ####
thresh=0.3
result['Review_Code']=result.apply(lambda x: check_review(x.Mode_Small_Model_Class, x.Mode_Small_Model_Class_Probability, x.top_level_topic,thresh ), axis=1) 

#### How much do we have in our first go around to be reviewed? ####
result_roll= result.groupby('External ID')['Review_Code'].apply(lambda x: x.value_counts().index[0]).reset_index()
to_review= result_roll['Review_Code'].value_counts()['HIGH']
print('Amount needing review, based on initial thresholds of: ', str(thresh), ' the amount of items to review are:', str(to_review))

# COMMAND ----------

#### Now we iterate until we get the right amount of items ####

while to_review> Decks_Needed:
  #run the review code
  result['Review_Code']=result.apply(lambda x: check_review(x.Mode_Small_Model_Class, x.Mode_Small_Model_Class_Probability, x.top_level_topic,thresh ), axis=1) 
  #roll up the results to get total decks
  result_roll= result.groupby('External ID')['Review_Code'].apply(lambda x: x.value_counts().index[0]).reset_index()
  to_review=result_roll['Review_Code'].value_counts()['HIGH']
  thresh=thresh+0.1
  
print('Final thresholds were:', thresh)
print('Amount of items to be reviewed are:', to_review)

# COMMAND ----------

#### These are the ids to review for sure ###
result_roll[result_roll.Review_Code=='HIGH']

# COMMAND ----------

#### These are the ids that need heuristics to determine if review is required ####
result_roll[result_roll.Review_Code=='HEURISTICS NEEDED']
